///scr_enemy_chase
state_text = "chase";
var dist = point_distance(x,y,oPlayer.x,oPlayer.y);
var dir = point_direction(x,y,oPlayer.x,oPlayer.y);


if (dist <= chaseRange and dist > atkRange){
    //motion_set(dir,spd)
    mp_potential_step(oPlayer.x,oPlayer.y,spd,false);
} else if (dist <= atkRange){
    speed = 0;
    state = states.enemyAttack;
} else {
    speed = 0;
    state = states.enemyIdle;
}
