var str = argument0;
var maxWidth = argument1;

var strLen = string_length(str);
var last_space = 1;

var count = 1;

repeat(strLen){
	substr = string_copy(str, 1, count);
	if(string_char_at(str, count) == " ") lastSpace = count;
	
	
	if (string_width(substr) > maxWidth){
		str = string_delete(str, last_space, 1);
		str = string_insert("\n", str, lastSpace);
	}
	count++;
}

return str;