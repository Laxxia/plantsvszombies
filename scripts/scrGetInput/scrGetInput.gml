///scr_get_input
up_key = keyboard_check(ord("W"))
left_key = keyboard_check(ord("A"))
down_key = keyboard_check(ord("S")) 
right_key = keyboard_check(ord("D")) 

ranged_key = mouse_check_button(mb_left);

debug_key = keyboard_check(ord("M"))
restart_key = keyboard_check(vk_escape);
