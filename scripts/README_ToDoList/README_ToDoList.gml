/* ______          ____            __     _        __ 
  /_  __/____     / __ \ ____     / /    (_)_____ / /_
   / /  / __ \   / / / // __ \   / /    / // ___// __/
  / /  / /_/ /  / /_/ // /_/ /  / /___ / /(__  )/ /_  
 /_/   \____/  /_____/ \____/  /_____//_//____/ \__/ 
-------------------------------------------------------
*/


#region //Player Stuff
/*-----------------------------------------------------------------

1. Create an energy/mana system
2. Add a melee attack, probably wouldnt cost energy
3. Maybe a movement ability (dodge roll or something)
4. I was thinking about a hold right click for a charged attack kind of thing

--------------------------------------------------------------------*/
#endregion


#region //Level Stuff
/*------------------------------------------------------------

1. I'd probably zoom the view out a little bit, fov seems a bit tight
2. Random spawning of chests?
3. Item system
4. This is for later down the road, but the class tree

--------------------------------------------------------------*/
#endregion


#region //Enemy Stuff
/*---------------------------------------------------------------------

1. Add enemy attack
2. Add item drops -> Would go with item system

--------------------------------------------------------------------*/
#endregion


//DON'T FORGET TO SAVE A BUNCH ;)

