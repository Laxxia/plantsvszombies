///scr_move_state
scrGetInput();
movement = MOVE;

//GetTheAxis
var xaxis = (right_key - left_key);
var yaxis = (down_key - up_key);

//GetDirection
dir = point_direction(0,0,xaxis,yaxis)

//GetLength
if (xaxis == 0 and yaxis == 0){
    len = 0;
}else{
    len = spd;
    scrGetFace();
}

//Get hspd and vspd
hspd = lengthdir_x(len,dir)
vspd = lengthdir_y(len,dir)

//Collision
if(place_meeting(round(x+hspd),round(y),oSolid)){
    while(!place_meeting(round(x+hspd),round(y),oSolid))x += sign(hsp);
    hspd = 0;
}

if(place_meeting(round(x),round(y +vspd),oSolid)){
    while(!place_meeting(round(x),round(y +vspd),oSolid))y += sign(vsp);
    vspd = 0;
}

//Movement
x = x + hspd;
y = y + vspd;


//Ranged Attack
if (attacked = false){
	if (ranged_key){
		instance_create_depth(x,y,0,oBullet);
		attacked = true;
		alarm[0] = room_speed*RangedCooldown;
	}
}


image_speed = imgspd;
if (len == 0) image_index = 0;
