{
    "id": "a80cafd8-6d97-4e78-9e2c-19d60bc70067",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oBunny",
    "eventList": [
        {
            "id": "542dbf88-979b-446f-8ba3-10eb942f257c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a80cafd8-6d97-4e78-9e2c-19d60bc70067"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "63246d80-4f08-45d7-9cdd-13c5d51c238d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "337c3643-626f-4eba-b8e0-63664a419eed",
    "visible": true
}