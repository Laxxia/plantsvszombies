/// @description Initialize Bunny
event_inherited();
spd = 1;

sightRange = 100;
chaseRange = 180;
atkRange = 35;

orbsToMake = 5;
goldToMake = 4;

hp = 5;