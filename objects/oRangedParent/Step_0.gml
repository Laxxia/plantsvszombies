///@description Collision
event_inherited();

#region //Solid Collision 
if(place_meeting(round(x+spd),round(y),oSolid)){
    while(!place_meeting(round(x+spd),round(y),oSolid))x += sign(hsp);
    spd = 0;
	instance_destroy();
}
if(place_meeting(round(x),round(y +spd),oSolid)){
    while(!place_meeting(round(x),round(y +spd),oSolid))y += sign(vsp);
    spd = 0;
	instance_destroy();
}
#endregion

#region //Enemy Collision
if(place_meeting(round(x+spd),round(y),oEnemyParent)){
    while(!place_meeting(round(x+spd),round(y),oEnemyParent))x += sign(hsp);
    spd = 0;
	oEnemyParent.hp -= dmg;
	instance_destroy();
}
if(place_meeting(round(x),round(y +spd),oEnemyParent)){
    while(!place_meeting(round(x),round(y +spd),oEnemyParent))y += sign(vsp);
    spd = 0;
	oEnemyParent.hp -= dmg;
	instance_destroy();
}
#endregion

//Enemy Damage