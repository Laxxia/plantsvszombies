{
    "id": "f588d9d7-e7ff-4e27-8ae8-3e069befed0e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRangedParent",
    "eventList": [
        {
            "id": "f30efe87-a6df-4fe3-b804-4667abce3158",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f588d9d7-e7ff-4e27-8ae8-3e069befed0e"
        },
        {
            "id": "e3ac8828-941f-47ab-88b8-eb4f56ed69a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f588d9d7-e7ff-4e27-8ae8-3e069befed0e"
        },
        {
            "id": "d245d945-1dff-4a72-9d29-fc2f811dfee2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f588d9d7-e7ff-4e27-8ae8-3e069befed0e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe101ecc-90c9-4d7a-85e3-22a3a52ba7f2",
    "visible": true
}