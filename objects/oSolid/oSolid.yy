{
    "id": "91d609bd-7292-4acc-b591-e593d85f8bb7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSolid",
    "eventList": [
        {
            "id": "c9b8f952-9a56-46f8-836e-5045fbebce03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "63246d80-4f08-45d7-9cdd-13c5d51c238d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "91d609bd-7292-4acc-b591-e593d85f8bb7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "73c0c09e-22a0-4cd8-bf5b-12037ab593cb",
    "visible": true
}