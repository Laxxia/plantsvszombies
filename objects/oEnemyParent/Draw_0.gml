///@description Debug
draw_self();
if(debug = true){
    draw_set_halign(fa_center);
    draw_set_valign(fa_middle);
    draw_set_alpha(.1);
    draw_circle_color(x,y,sightRange,c_red,c_red,false);
    draw_circle_color(x,y,atkRange,c_red,c_red,false);
    draw_text(x,y,state_text);
    if (state = states.enemyChase){
        draw_circle_color(x,y,chaseRange,c_red,c_red,false);
    }
    draw_set_alpha(1);
}

