{
    "id": "63246d80-4f08-45d7-9cdd-13c5d51c238d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oEnemyParent",
    "eventList": [
        {
            "id": "af7ee92f-ccd0-45fc-840a-3a95cd149096",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63246d80-4f08-45d7-9cdd-13c5d51c238d"
        },
        {
            "id": "feec03f9-ea99-4bca-92e6-78ca7da1600a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "63246d80-4f08-45d7-9cdd-13c5d51c238d"
        },
        {
            "id": "9da8684b-8e6f-4e9a-8e1a-e50920dc323e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "63246d80-4f08-45d7-9cdd-13c5d51c238d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}