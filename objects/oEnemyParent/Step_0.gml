/// @description Enemy States and Collision
event_inherited();

//Drop XP
if (hp <= 0) {
	orbsCreated = 0
	//orbsToMake decided by individual enemy type, not on parent.
	while (orbsCreated < orbsToMake){
		instance_create_depth(x+random_range(-15,15), y+random_range(-15,15),-100,oExpOrb);
		orbsCreated ++;
	}
	instance_destroy();
}

//Drop Gold
if (hp <= 0) {
	goldCreated = 0
	//goldToMake decided by individual enemy type, not on parent.
	while (goldCreated < goldToMake){
		instance_create_depth(x+random_range(-15,15), y+random_range(-15,15),-100,oGold);
		goldCreated ++;
	}
	instance_destroy();
}


//Collision Check
if(place_meeting(round(x+hsp),round(y),oSolid)){
	while(!place_meeting(round(x+hsp),round(y),oSolid))x += sign(hsp);
	hsp = 0;
}

x += hsp;

if(place_meeting(round(x),round(y +vsp),oSolid)){
	while(!place_meeting(round(x),round(y +vsp),oSolid))y += sign(vsp);
	vsp = 0;
}
y += vsp;




//Line of Sight
los = collision_line(x,y,oPlayer.x,oPlayer.y,oSolid,false,true) = noone;



switch(state){
	case states.enemyIdle:
		scrEnemyIdle();
		//make blue in idle
		image_blend = make_colour_rgb(0, 0, 255);
		break;
	case states.enemyChase:
		scrEnemyChase();
		//make green in chase
		image_blend = make_color_rgb(0,255,0)
		break;
	case states.enemyAttack:
		scrEnemyAttack();
		//make red when in atk
		image_blend = make_colour_rgb(255, 0, 0);
		break;
}




