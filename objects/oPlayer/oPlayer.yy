{
    "id": "0819c130-afdd-43f1-a9c9-fb5ffe62fbc5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "c28ece8c-e318-4dc5-88e0-3467b16d418b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0819c130-afdd-43f1-a9c9-fb5ffe62fbc5"
        },
        {
            "id": "77211477-6044-4fa8-94fa-f05eaa5ea129",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0819c130-afdd-43f1-a9c9-fb5ffe62fbc5"
        },
        {
            "id": "14c406da-7cba-4155-9a2e-f2edbef6ecc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0819c130-afdd-43f1-a9c9-fb5ffe62fbc5"
        },
        {
            "id": "98584470-f27d-4e7e-a43b-6f5e05a354fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0819c130-afdd-43f1-a9c9-fb5ffe62fbc5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c018b1c1-c8ee-45c2-a53e-ee3637c4540c",
    "visible": true
}