/// @description Initialize Player
hp = 10;
if (instance_number(oPlayer)) > 1 instance_destroy();

//Test Movement variables
x_speed_ = 0;
y_speed_ = 0;
max_speed_ = 2;
acceleration_ = .5;

#region //Old Movement Stuff
//NO longer test variables
spd = 3;
imgspd = .3;
hspd = 0;
vspd = 0;
len = 0;
dir = 0;
#endregion

face = RIGHT;
scrGetInput();
state = scrMoveState;
movement = MOVE;

xp = 0;
gold = 0;

attacked = false;
RangedCooldown = 1;

instance_create_layer(x, y, "Instances", oCamera);

#macro RIGHT 0;
#macro UP 1;
#macro LEFT 2;
#macro DOWN 3;
#macro MOVE 0
