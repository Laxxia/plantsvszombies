///@description States & Animation
depth = -y;
event_inherited();

script_execute(state);


if (hp = 0){
    game_restart();
}    

if (instance_exists(oEnemyParent)){
	if (debug_key) oEnemyParent.debug = true;
	if keyboard_check_released(debug_key) oEnemyParent.debug = false;
}
if (restart_key) game_restart();

