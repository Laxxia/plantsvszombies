{
    "id": "9e3129fe-3ad6-4df2-995c-01231bbae295",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGold",
    "eventList": [
        {
            "id": "64ba3465-0ce9-4784-a56e-48f353d65d19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e3129fe-3ad6-4df2-995c-01231bbae295"
        },
        {
            "id": "81716998-86c6-4219-ac9c-8499e52606ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e3129fe-3ad6-4df2-995c-01231bbae295"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1090e744-b5d6-4490-b9ba-4ac05cbf39ab",
    "visible": true
}