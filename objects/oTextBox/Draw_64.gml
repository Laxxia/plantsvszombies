/// @description Draw text
// You can write your code in this editor
draw_sprite(box, 0, boxX, boxY);

draw_sprite(frame, 0, portX, portY);

draw_sprite(portrait, portraitIndex, portX, portY);

draw_sprite(frame, 1, portX, portY);

draw_sprite(namebox, 0, nameboxX, nameboxY);

var c = nameTextCol; 
draw_set_halign(fa_center); draw_set_valign(fa_middle);

draw_text_color(nameTextX, nameTextY, name, c,c,c,c, 1);
draw_set_halign(fa_left); draw_set_valign(fa_top);

//draw text
if(counter < strLen){
	counter++;
}
var substr = string_copy(textWrapped, 1, counter);

draw_text_color(textX, textY, substr, c,c,c,c, 1);
