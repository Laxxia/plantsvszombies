{
    "id": "cf109982-6ab4-45c5-a5b0-85893028d796",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTextBox",
    "eventList": [
        {
            "id": "a7a4f780-0e73-4a9e-807b-ef722760a759",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "cf109982-6ab4-45c5-a5b0-85893028d796"
        },
        {
            "id": "1a93708e-cbe4-4465-9f17-9bbbf54feaf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf109982-6ab4-45c5-a5b0-85893028d796"
        },
        {
            "id": "4966945c-846d-4f1b-b59c-1bffee8b041a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cf109982-6ab4-45c5-a5b0-85893028d796"
        },
        {
            "id": "3f68ed03-85fa-446b-bf4f-3bc00e2d3367",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "cf109982-6ab4-45c5-a5b0-85893028d796"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}