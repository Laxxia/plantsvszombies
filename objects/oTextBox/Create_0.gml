/// @description Deciding Box
// You can write your code in this editor
box = sTextBox;
frame = sPortraitFrame;
portrait = sPortraits;
namebox = sNameBox;

portraitIndex = 0;

boxWidth = sprite_get_width(box);
bowHeight = sprite_get_height(box);
portWidth = sprite_get_width(portrait);
portHeight = sprite_get_height(portrait);
nameWidth = sprite_get_width(namebox);
nameHeight = sprite_get_height(namebox);

portX = (1024 - boxWidth - portWidth) * .5;
portY = (768 * .98) - portHeight;

boxX = portX + portWidth;
boxY = portY

nameboxX = portX;
nameboxY = boxY - nameHeight;

xBuffer = 12;
yBuffer = 8;

textX = boxX + xBuffer;
textY = boxY + yBuffer;

nameTextX = nameboxX + (nameWidth/2);
nameTextY = nameboxY + (nameHeight/2);

textMaxWidth = boxWidth - (2*xBuffer);
textMaxHeight = string_height("M");

text[0] = "This is a test string This is a test string This is a test string This is a test string This is a test string This is a test string";
text[1] = "This is the second page";
page = 0;
name = "name";

interactKey = ord("E");

textCol = c_black;
nameTextCol = c_black;
counter = 0;
event_perform(ev_other, ev_user1);
