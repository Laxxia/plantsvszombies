{
    "id": "7137e2c9-a216-4779-b2fd-b1385eda8793",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oExpOrb",
    "eventList": [
        {
            "id": "53329490-ee03-4ff4-9031-57b2122e1717",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7137e2c9-a216-4779-b2fd-b1385eda8793"
        },
        {
            "id": "25ad2423-939b-43d3-88a5-4e2dea47d924",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7137e2c9-a216-4779-b2fd-b1385eda8793"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e85d1ff-19ff-497a-b0d7-d0116276daa9",
    "visible": true
}