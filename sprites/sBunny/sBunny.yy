{
    "id": "337c3643-626f-4eba-b8e0-63664a419eed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBunny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 15,
    "bbox_right": 51,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21d19394-ac36-407f-909e-746747192594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "337c3643-626f-4eba-b8e0-63664a419eed",
            "compositeImage": {
                "id": "d6f69177-92a2-42e0-b752-0a144cfc1046",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21d19394-ac36-407f-909e-746747192594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f3d6c3-fc62-4caa-bb33-5f8b686e578f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21d19394-ac36-407f-909e-746747192594",
                    "LayerId": "8e7665b6-146a-4e3f-a20a-c391f527b928"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8e7665b6-146a-4e3f-a20a-c391f527b928",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "337c3643-626f-4eba-b8e0-63664a419eed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}