{
    "id": "0e85d1ff-19ff-497a-b0d7-d0116276daa9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sExpOrb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f979c51-0074-403f-8164-90ab63462ef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e85d1ff-19ff-497a-b0d7-d0116276daa9",
            "compositeImage": {
                "id": "a0c9fbab-d7aa-4c98-a927-dda31e5f9aaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f979c51-0074-403f-8164-90ab63462ef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "586e05e3-4183-4f80-84c9-d94743417383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f979c51-0074-403f-8164-90ab63462ef8",
                    "LayerId": "1790559e-2f8d-4a09-8dc2-626542e7bf17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1790559e-2f8d-4a09-8dc2-626542e7bf17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e85d1ff-19ff-497a-b0d7-d0116276daa9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}