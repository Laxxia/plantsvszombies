{
    "id": "f7c18a45-1f74-43e5-9133-475cfd3f63ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61bfc043-a96e-407b-9c93-269f8b393a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7c18a45-1f74-43e5-9133-475cfd3f63ce",
            "compositeImage": {
                "id": "163f3227-738a-4dde-814a-15db76d3cd0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61bfc043-a96e-407b-9c93-269f8b393a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba25df1-cfc7-4ffa-8c92-219c9e553524",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61bfc043-a96e-407b-9c93-269f8b393a9f",
                    "LayerId": "c71bd37f-509b-4eac-bf06-d326d900bb70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c71bd37f-509b-4eac-bf06-d326d900bb70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7c18a45-1f74-43e5-9133-475cfd3f63ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}