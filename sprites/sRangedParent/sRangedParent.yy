{
    "id": "fe101ecc-90c9-4d7a-85e3-22a3a52ba7f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sRangedParent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "696ce6c0-6a29-49f0-b09b-bf671e598afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe101ecc-90c9-4d7a-85e3-22a3a52ba7f2",
            "compositeImage": {
                "id": "b975ede2-d52c-4399-be7c-720b27296f1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "696ce6c0-6a29-49f0-b09b-bf671e598afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ed68b2c-c092-42ca-9220-39f08a4a4367",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "696ce6c0-6a29-49f0-b09b-bf671e598afb",
                    "LayerId": "0df9df92-35e8-4abc-bb40-55782d4fee87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0df9df92-35e8-4abc-bb40-55782d4fee87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe101ecc-90c9-4d7a-85e3-22a3a52ba7f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}