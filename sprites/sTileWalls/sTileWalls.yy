{
    "id": "7df982e1-aefd-4257-b4b1-74a7119d3219",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileWalls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16f72046-fa9d-4d9f-b0d9-eb80231e76bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7df982e1-aefd-4257-b4b1-74a7119d3219",
            "compositeImage": {
                "id": "044c703d-a6be-4985-ba72-11062fd8485c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f72046-fa9d-4d9f-b0d9-eb80231e76bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d877a01-dc3d-40d4-8ab9-b0fcdfa458f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f72046-fa9d-4d9f-b0d9-eb80231e76bb",
                    "LayerId": "43fa628c-f810-40d9-b23f-faa84b48354d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "43fa628c-f810-40d9-b23f-faa84b48354d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7df982e1-aefd-4257-b4b1-74a7119d3219",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}