{
    "id": "0af3214e-412c-490e-a32a-6ad87882d5f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTextBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7dcfb5f-9adc-4798-8d12-88b31c0cc22f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0af3214e-412c-490e-a32a-6ad87882d5f8",
            "compositeImage": {
                "id": "585c9be4-bb26-479f-8122-36b51b6d2a8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7dcfb5f-9adc-4798-8d12-88b31c0cc22f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba8434ed-56c0-4c6b-a826-58ec3b863b1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7dcfb5f-9adc-4798-8d12-88b31c0cc22f",
                    "LayerId": "a01dce32-17be-4ad9-9905-e0580efd7971"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a01dce32-17be-4ad9-9905-e0580efd7971",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0af3214e-412c-490e-a32a-6ad87882d5f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}