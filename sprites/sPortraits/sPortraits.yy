{
    "id": "0eea0300-b78d-4a3a-894d-310655fb7790",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPortraits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 1,
    "bbox_right": 123,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c40b291-2a67-4b01-baa9-4a870824d858",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "compositeImage": {
                "id": "406214d2-1d19-4fe2-b92f-eee81cefc72f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c40b291-2a67-4b01-baa9-4a870824d858",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8cdfe00-2a2b-4487-a9f8-af8a6098408a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c40b291-2a67-4b01-baa9-4a870824d858",
                    "LayerId": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2"
                }
            ]
        },
        {
            "id": "3b88e303-ad44-42b2-b4df-f9d30138111e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "compositeImage": {
                "id": "c013f61a-fe67-4b44-b93a-3132cc027129",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b88e303-ad44-42b2-b4df-f9d30138111e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f30af6c9-919a-40d9-ac6b-833ac48c6773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b88e303-ad44-42b2-b4df-f9d30138111e",
                    "LayerId": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2"
                }
            ]
        },
        {
            "id": "8340acc7-96a2-4221-bb7d-348481347b94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "compositeImage": {
                "id": "e04cef93-3794-4579-91da-bf68e2046caf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8340acc7-96a2-4221-bb7d-348481347b94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d05dfbd-8acd-4b20-9349-aaa8b337a102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8340acc7-96a2-4221-bb7d-348481347b94",
                    "LayerId": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2"
                }
            ]
        },
        {
            "id": "4ceff463-ff50-4669-afca-9b3d1f922d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "compositeImage": {
                "id": "5c4f864a-21c3-479c-b0dd-f8faa4be5f9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ceff463-ff50-4669-afca-9b3d1f922d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30563d6a-8885-49af-9596-03baf1dcf0b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ceff463-ff50-4669-afca-9b3d1f922d38",
                    "LayerId": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2"
                }
            ]
        },
        {
            "id": "f6599517-f9b7-49ab-a9f7-a0e852b978f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "compositeImage": {
                "id": "f15045e3-bd0c-4727-a53f-183c12dccfa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6599517-f9b7-49ab-a9f7-a0e852b978f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f37dd4ad-3d5f-4b23-88fe-9f4410a9d45e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6599517-f9b7-49ab-a9f7-a0e852b978f1",
                    "LayerId": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2"
                }
            ]
        },
        {
            "id": "688e1994-cc96-4a1d-b9a0-3b76ceefba58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "compositeImage": {
                "id": "4bffdcfe-8d0a-4dcf-9bbe-2c218cae4310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "688e1994-cc96-4a1d-b9a0-3b76ceefba58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5ae2c48-8bf0-4846-bcfa-736ec33c8878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "688e1994-cc96-4a1d-b9a0-3b76ceefba58",
                    "LayerId": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2"
                }
            ]
        },
        {
            "id": "72b12263-104e-40c0-9c66-464b7f20b454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "compositeImage": {
                "id": "763d8fbb-6c14-4960-8f8e-bd42162cc57a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72b12263-104e-40c0-9c66-464b7f20b454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c7420ce-a80b-4e55-8868-d1d90a1de8b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72b12263-104e-40c0-9c66-464b7f20b454",
                    "LayerId": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2"
                }
            ]
        },
        {
            "id": "0ee6a14b-5b30-4ded-824a-2b17d4d2aea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "compositeImage": {
                "id": "9296fb7d-dcbb-4406-bb61-7d85e8928f96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ee6a14b-5b30-4ded-824a-2b17d4d2aea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6fec610-7672-4c78-b80e-b602f6fd2f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ee6a14b-5b30-4ded-824a-2b17d4d2aea7",
                    "LayerId": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "bf1f6d2d-8ac0-491d-814f-861caebdbbd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0eea0300-b78d-4a3a-894d-310655fb7790",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}