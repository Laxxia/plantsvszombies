{
    "id": "86df8724-1059-4abd-8091-419b230503f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPortraitFrame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75de01fa-1a35-4ab4-8487-755cf5607fe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86df8724-1059-4abd-8091-419b230503f2",
            "compositeImage": {
                "id": "b98d246b-60a4-4910-9aee-046d9d39f105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75de01fa-1a35-4ab4-8487-755cf5607fe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d48aea-6cbc-4072-99e6-20b52404c6bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75de01fa-1a35-4ab4-8487-755cf5607fe3",
                    "LayerId": "4cab8f5b-0f0e-469f-ac86-de9c36feccaa"
                }
            ]
        },
        {
            "id": "97cea1af-6c97-45a7-9466-c4da2698149f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86df8724-1059-4abd-8091-419b230503f2",
            "compositeImage": {
                "id": "65ed88d6-9dfd-4961-a898-66d8333cae74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97cea1af-6c97-45a7-9466-c4da2698149f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd7c5aa7-f9cb-41a6-baba-4d2b0ddfba73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97cea1af-6c97-45a7-9466-c4da2698149f",
                    "LayerId": "4cab8f5b-0f0e-469f-ac86-de9c36feccaa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4cab8f5b-0f0e-469f-ac86-de9c36feccaa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86df8724-1059-4abd-8091-419b230503f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}