{
    "id": "1297877c-ec7e-4e0f-9f07-df3dcf52e1c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sNameBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96294ba0-59be-4c88-a210-e2824794e7ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1297877c-ec7e-4e0f-9f07-df3dcf52e1c8",
            "compositeImage": {
                "id": "00b964e5-167e-40f7-8a0f-59d615f6dcaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96294ba0-59be-4c88-a210-e2824794e7ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ed6bb38-9567-48f8-a59b-cc6514805089",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96294ba0-59be-4c88-a210-e2824794e7ac",
                    "LayerId": "1fae24c6-cdbc-4fac-850f-7ff2d1d48983"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1fae24c6-cdbc-4fac-850f-7ff2d1d48983",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1297877c-ec7e-4e0f-9f07-df3dcf52e1c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}