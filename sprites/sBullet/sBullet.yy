{
    "id": "92666fa2-2511-46b9-9771-cb8eb9354488",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6c38e1b-d445-46cb-bea1-44c8ab5bc25b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92666fa2-2511-46b9-9771-cb8eb9354488",
            "compositeImage": {
                "id": "151ed631-b59a-4d21-a9c0-b20cd5eb1e8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c38e1b-d445-46cb-bea1-44c8ab5bc25b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e2e8e1b-e330-496c-8c0b-783eccda56ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c38e1b-d445-46cb-bea1-44c8ab5bc25b",
                    "LayerId": "b6a555cc-21c6-4812-bfd0-ada91ec6eaa9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b6a555cc-21c6-4812-bfd0-ada91ec6eaa9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92666fa2-2511-46b9-9771-cb8eb9354488",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}