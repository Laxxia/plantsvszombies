{
    "id": "73c0c09e-22a0-4cd8-bf5b-12037ab593cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86349aeb-5237-4437-8a74-95c675aed87f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73c0c09e-22a0-4cd8-bf5b-12037ab593cb",
            "compositeImage": {
                "id": "954a87f2-5074-491c-9f05-d14d6461e106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86349aeb-5237-4437-8a74-95c675aed87f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46d09bfc-9e32-4372-bf40-b174eed3882c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86349aeb-5237-4437-8a74-95c675aed87f",
                    "LayerId": "f73d1c7e-dac8-4b7b-8710-40bcaeb89423"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f73d1c7e-dac8-4b7b-8710-40bcaeb89423",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73c0c09e-22a0-4cd8-bf5b-12037ab593cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}