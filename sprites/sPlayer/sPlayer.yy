{
    "id": "c018b1c1-c8ee-45c2-a53e-ee3637c4540c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 24,
    "bbox_right": 40,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ef43dbd-aae7-4ba0-97a0-162773c3342a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c018b1c1-c8ee-45c2-a53e-ee3637c4540c",
            "compositeImage": {
                "id": "db271c62-824c-4214-ba1f-9a5a14e906e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ef43dbd-aae7-4ba0-97a0-162773c3342a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd80ea9f-6106-4291-b1fc-289f28f939d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ef43dbd-aae7-4ba0-97a0-162773c3342a",
                    "LayerId": "edf0c257-80e4-4423-a3f3-f1ab7851e258"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "edf0c257-80e4-4423-a3f3-f1ab7851e258",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c018b1c1-c8ee-45c2-a53e-ee3637c4540c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}